package com.ringover.test

import android.app.Application

class CallApplication : Application() {
    companion object {
        lateinit var instance: CallApplication
            private set

    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}