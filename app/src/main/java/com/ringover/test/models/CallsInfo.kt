package com.ringover.test.models

import com.google.gson.annotations.SerializedName

data class CallsInfo(@SerializedName("missed_calls") val missedCalls: Int, val voicemails: Int, val sms: Int)