package com.ringover.test.models

data class Call(val name: String, val number: String, val info: String, val favorite: Boolean, val date: Long)