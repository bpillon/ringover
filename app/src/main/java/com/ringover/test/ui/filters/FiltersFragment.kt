package com.ringover.test.ui.filters

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.ViewGroup
import com.ringover.test.R

class FiltersFragment : Fragment() {
    companion object {
        fun newInstance() = FiltersFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.filters_fragment, container, false)

}