package com.ringover.test.ui.main

import android.view.LayoutInflater
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.ringover.test.misc.CallClickedListener
import com.ringover.test.R
import com.ringover.test.misc.getDate
import com.ringover.test.models.Call
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.call_adapter.view.*

class CallAdapter(val listener: CallClickedListener) : RecyclerView.Adapter<CallAdapter.ViewHolder>() {

    var calls: List<Call> = ArrayList()

    fun updateCalls(calls: List<Call>) {
        this.calls = calls
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.call_adapter, parent, false)
    )

    override fun getItemCount() = calls.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(calls[position], listener)

    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(call: Call, listener: CallClickedListener) = with(itemView) {
            name.text = call.name
            info.text = call.info
            number.text = call.number
            date.text = call.date.getDate()
            favourite.isChecked = call.favorite

            setOnClickListener {
                listener.onCallClicked(call)
            }
        }
    }
}