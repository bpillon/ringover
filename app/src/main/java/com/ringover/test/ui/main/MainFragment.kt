package com.ringover.test.ui.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ringover.test.misc.CallClickedListener
import com.ringover.test.R
import com.ringover.test.models.Call
import com.ringover.test.models.CallsInfo
import com.ringover.test.ui.detail.DetailFragment
import com.ringover.test.ui.filters.FiltersFragment
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment(), CallClickedListener {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(MainViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.main_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapter = setupList()

        viewModel.calls.observe(this, Observer<List<Call>> { calls ->
            calls?.let {
                adapter.updateCalls(it)
            }
        })

        viewModel.callsInfo.observe(this, Observer<CallsInfo> { callsInfo ->
            callsInfo?.let {
                missed_calls_count.text = it.missedCalls.toString()
                voicemails_count.text = it.voicemails.toString()
                sms_count.text = it.sms.toString()
            }
        })

        filters.setOnClickListener {
            updateFragment(FiltersFragment.newInstance())
        }
    }

    private fun setupList(): CallAdapter {
        list.layoutManager = LinearLayoutManager(context)
        val adapter = CallAdapter(this)
        list.adapter = adapter
        list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        return adapter
    }

    override fun onCallClicked(call: Call) {
        updateFragment(DetailFragment.newInstance())
    }

    private fun updateFragment(fragment: Fragment) {
        activity?.let {
            it.supportFragmentManager.beginTransaction().replace(R.id.container, fragment)
                .addToBackStack(null).commit()
        }
    }
}
