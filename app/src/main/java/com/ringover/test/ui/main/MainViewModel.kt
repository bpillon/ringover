package com.ringover.test.ui.main

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.support.annotation.RawRes
import com.google.gson.Gson
import com.ringover.test.models.Call
import com.google.gson.reflect.TypeToken
import com.ringover.test.CallApplication
import com.ringover.test.R
import com.ringover.test.models.CallsInfo

class MainViewModel : ViewModel() {
    val calls: MutableLiveData<List<Call>> = MutableLiveData()
    val callsInfo: MutableLiveData<CallsInfo> = MutableLiveData()

    init {
        getCalls()
        getCallsInfo()
    }

    private fun getCalls() {
        calls.value = mapJson(R.raw.calls, object : TypeToken<List<Call>>(){})
    }

    private fun getCallsInfo() {
        callsInfo.value = mapJson(R.raw.callsinfo, object : TypeToken<CallsInfo>(){})
    }

    private fun <T> mapJson(@RawRes resId: Int, typeToken: TypeToken<T>) : T {
        val json = CallApplication.instance.resources.openRawResource(resId).bufferedReader().use { it.readText() }
        return Gson().fromJson(json, typeToken.type)
    }
}
