package com.ringover.test.misc

import com.ringover.test.models.Call
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

interface CallClickedListener : Serializable {
    fun onCallClicked(call: Call)
}

fun Long.getDate(): String {
    val date = Date(this)
    val sdf = SimpleDateFormat("dd MMM, hh:mm")

    return sdf.format(date)
}